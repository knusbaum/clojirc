(defproject clojirc "0.1.0-SNAPSHOT"
  :description "An IRC client written in clojure"
  :url "http://github.com/Jack-Rabbit/clojirc"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [aleph "0.3.0-beta16"]
                 [seesaw "1.4.3"]]
  :main clojirc.core
  :java-source-paths ["src/java"])
