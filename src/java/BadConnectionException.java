/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clojircui;

/**
 *
 * @author kyle
 */
public class BadConnectionException extends Exception{
    public BadConnectionException() {super();}
    public BadConnectionException(String message) {super(message);}
    public BadConnectionException(String message, Throwable cause) {super(message, cause);}
    public BadConnectionException(Throwable cause) {super(cause);}
}
