/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clojircui;

import java.awt.Color;
import java.util.*;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

/**
 *
 * @author kyle
 */
public class ClojircGUI extends javax.swing.JFrame {
    private AttributeSet newChar;

    /**
     * Creates new form ClojircGUI
     */
    public ClojircGUI() {
        initComponents();
        connectEvents = new ArrayList<ClojircConnectEvent>();
        
        //ChannelListModel = new DefaultListModel();
        //ChannelList.setModel(ChannelListModel);
        
        textPanes = new HashMap<String, JTextPane>();
        textPanes.put("main", Output);
        
        
        ChannelTreeRoot = new DefaultMutableTreeNode("Servers");
        ChannelTreeModel = new DefaultTreeModel(ChannelTreeRoot);
        ChannelTree.setModel(ChannelTreeModel);        
    }

    public javax.swing.JButton GetSubmit() {return Submit;}
    public javax.swing.JTextField GetSubmitField() {return SubmitField;}
    public void AddConnectListener(ClojircConnectEvent event){
        connectEvents.add(event);
    }
    public void doConnect(ConnectionDescription cd){
        //JOptionPane.showMessageDialog(this, "MESSAGE");
        AddServer(cd.Server);
        for(int i = 0; i < connectEvents.size(); i++)
        {
            connectEvents.get(i).Connect(cd);
        }
//        ChannelList.add(new javax.swing.JLabel("Helloo!"));
//        ChannelListModel.addElement(cd.Server);
        
    }
    public void SubmitMessageToChannel(String server, String channel, String user, String text){
        JTextPane chan = textPanes.get(channel);
        if(chan == null)
        {
            if(server == null)
            {
                System.out.println("Creating server for ".concat(channel));
                AddServer(channel);
                chan = textPanes.get(channel);
            }
            else
            {
                System.out.println("Creating channel for ".concat(channel).concat(" on ").concat(server));
                AddChannelToServer(server, channel);
                chan = textPanes.get(channel);
            }
        }
        chan.setCaretPosition(chan.getDocument().getLength());
        
        StyledDocument doc = chan.getStyledDocument();
        Style style = chan.addStyle("Test-Style", null);
        
        if(user != null)
        {    
            StyleConstants.setForeground(style, new Color(user.hashCode()));
            String markedUser = user.concat(": ");
            try { doc.insertString(doc.getLength(), 
                                   markedUser.concat(
                                    new String(
                                        new char[Math.max(11 - markedUser.length(), 1)])
                                        .replace('\0', ' ')), 
                                   style); } 
            catch (BadLocationException e){}
        }
        else
        {
            StyleConstants.setForeground(style, Color.BLACK);
            try { doc.insertString(doc.getLength(), new String(new char[11]).replace('\0', ' '), style);}
            catch (BadLocationException e) {}
        }
        
        StyleConstants.setForeground(style, Color.BLACK);
        try { doc.insertString(doc.getLength(), text.concat("\n"), style); }
        catch (BadLocationException e) {}
        
        /*
        Style style = chan.addStyle("I'm a Style", null);
        StyleConstants.setForeground(style, Color.red);

        try { doc.insertString(doc.getLength(), "BLAH ",style); }
        catch (BadLocationException e){}

        StyleConstants.setForeground(style, Color.blue);

        try { doc.insertString(doc.getLength(), "BLEH",style); }
        catch (BadLocationException e){}
        
        Color x = new Color(1234);
        */
        
        //textPanes.get(channel). .append(text.concat("\n"));
        chan.setCaretPosition(chan.getDocument().getLength());
    }
    public void AddServer(String server)
    {
        ChannelTreeModel.insertNodeInto(
                    new DefaultMutableTreeNode(server), 
                    ChannelTreeRoot, 
                    ChannelTreeRoot.getChildCount());
        ChannelTree.expandRow(0);
        
        addTextAreaFor(server);
        SubmitMessageToChannel(null, server, null, "Submitting text!");
    }
    public void AddChannelToServer(String server, String channel)
    {
        Enumeration servers = ChannelTreeRoot.children();
        while(servers.hasMoreElements())
        {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode)servers.nextElement();
            if(child.toString().equals(server))
            {
                ChannelTreeModel.insertNodeInto(new DefaultMutableTreeNode(channel), child, 0);
                break;
            }
            //JOptionPane.showMessageDialog(this, child, "child", JOptionPane.INFORMATION_MESSAGE);
        }
        addTextAreaFor(channel);
        SubmitMessageToChannel(server, channel, null, "Joined ".concat(channel).concat("\n"));
    }
    private void addTextAreaFor(String name)
    {
        JTextPane dup = new JTextPane();
        dup.setEditable(false);
        //dup.setText(name.concat("\n"));
        dup.setVisible(true);
        textPanes.put(name, dup);
        System.out.println("Added text area for ".concat(name));
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SubmitField = new javax.swing.JTextField();
        Submit = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        ChannelTree = new javax.swing.JTree();
        OutputPane = new javax.swing.JScrollPane();
        Output = new javax.swing.JTextPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        ConnectTo = new javax.swing.JMenuItem();
        JoinChannel = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Submit.setText("Submit");

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        ChannelTree.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        ChannelTree.setMaximumSize(new java.awt.Dimension(50, 20000));
        ChannelTree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ChannelTreeMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(ChannelTree);

        Output.setEditable(false);
        OutputPane.setViewportView(Output);

        jMenu1.setText("File");

        ConnectTo.setText("Connect to...");
        ConnectTo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConnectToActionPerformed(evt);
            }
        });
        jMenu1.add(ConnectTo);

        JoinChannel.setText("Join Channel");
        JoinChannel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                JoinChannelMouseReleased(evt);
            }
        });
        jMenu1.add(JoinChannel);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Submit, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(SubmitField)
                    .addComponent(OutputPane, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(OutputPane)
                    .addComponent(jScrollPane3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SubmitField)
                    .addComponent(Submit))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ConnectToActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConnectToActionPerformed
        // TODO add your handling code here:
        ConnectUI connectScreen = new ConnectUI(this);
        connectScreen.setVisible(true);        
    }//GEN-LAST:event_ConnectToActionPerformed

    private void ChannelTreeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ChannelTreeMouseClicked
        // TODO add your handling code here:
        TreePath tp = ChannelTree.getPathForLocation(evt.getX(),evt.getY());
        if(tp != null)
        {
            OutputPane.setViewportView(textPanes.get(tp.getLastPathComponent().toString()));
        }
    }//GEN-LAST:event_ChannelTreeMouseClicked

    private void JoinChannelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JoinChannelMouseReleased
        // TODO add your handling code here:
        AddChannelToServer("test", "newChannel");
    }//GEN-LAST:event_JoinChannelMouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClojircGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClojircGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClojircGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClojircGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClojircGUI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree ChannelTree;
    private javax.swing.JMenuItem ConnectTo;
    private javax.swing.JMenuItem JoinChannel;
    private javax.swing.JTextPane Output;
    private javax.swing.JScrollPane OutputPane;
    private javax.swing.JButton Submit;
    private javax.swing.JTextField SubmitField;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane3;
    // End of variables declaration//GEN-END:variables

    private ArrayList<ClojircConnectEvent> connectEvents;
    private Map<String, JTextPane> textPanes;
    //private DefaultListModel ChannelListModel;
    private DefaultTreeModel ChannelTreeModel;
    private DefaultMutableTreeNode ChannelTreeRoot;
}
