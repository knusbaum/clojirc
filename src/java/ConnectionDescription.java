/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clojircui;

import javax.swing.JOptionPane;

/**
 *
 * @author kyle
 */
public class ConnectionDescription {
    public String Server;
    public Integer Port;
    public String Nick;
    public String RealName;
    
    public ConnectionDescription(String server, 
                                    String port, 
                                    String nick, 
                                    String realName) throws BadConnectionException
    {
        
            Server = server;
            Port = Integer.parseInt(port);
            Nick = nick;
            RealName = realName;
            if(Server.equals("") || Nick.equals("") || RealName.equals("") || Port < 0 || Port > 65535)
            {
                throw new BadConnectionException();      
            }
    }
}
