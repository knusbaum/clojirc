(ns clojirc.ui
  (:import clojircui.ClojircGUI clojircui.ClojircConnectEvent)
  (:use lamina.core))

(declare add-output-channel)
(declare -register-submit-fn)


(defn setup-window []
  (let [window (ClojircGUI.)]
    (.setVisible window true)
    (let [submit-channel (channel)]
      (-register-submit-fn #(enqueue submit-channel %) window)
      (-> {} (assoc :window window) (assoc :submit submit-channel)))))

(defn -register-submit-fn [fn window]
  (let [submit (.GetSubmit window)
        submit-field (.GetSubmitField window)]
    (.. submit
        (addMouseListener
         (proxy [java.awt.event.MouseAdapter] []
           (mouseReleased [e] (do (fn (.getText submit-field))
                                  (.setText submit-field ""))))))))

(defn -parse-connection-description [cd]
  (-> {} 
      (assoc :server (.Server cd))
      (assoc :port (.Port cd)) 
      (assoc :nick (.Nick cd)) 
      (assoc :real-name (.RealName cd))))

(defn register-connect-to-fn 
  "Accepts a function and a window.
  The function itself accepts two arguments:
  A map representing a connection description, containing the keys:
  :server, :port, :nick, and :real-name
  The second argument is a channel which you can use to put text into the
  new server channel."
  [func window]
  (println "Registering connect-to function:" func window)
  (.. window
      (AddConnectListener
       (proxy [ClojircConnectEvent] []
         (Connect [cd] (let [parsed-cd 
                              (-parse-connection-description cd)]
                          (func parsed-cd
                                (add-output-channel 
                                 nil (get parsed-cd :server) window))))))))


(let [output-channels (atom {})]

  (defn add-output-channel [server chan-name window]
    (let [chan (channel)]
      (println 
       (str "Creating callback to SubmitMessageToChannel(" server "," name ", message)"))
      (receive-all chan 
                   #(.SubmitMessageToChannel window server chan-name (first %) (second %)))
      (swap! output-channels #(assoc % chan-name chan))
      chan))
  
  (defn get-output-channel [name]
    (get output-channels name)))

