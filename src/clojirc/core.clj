(ns clojirc.core
  (:gen-class)
  (:require clojirc.irc clojirc.ui clojure.java.io)
  (:use lamina.core aleph.tcp gloss.core))

;(declare user-submited)
;(declare message-handler)
;(declare window)

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  ;; work around dangerous default behaviour in Clojure
  ;(alter-var-root #'*read-eval* (constantly false))


  (let [window-submit (clojirc.ui/setup-window)]
    (clojirc.ui/register-connect-to-fn
     #(let [cd %1
            server-output %2]
        (println "Got Connection to:\n"
                 "Server:" (get cd :server)
                 "Port:" (get cd :port)
                 "Nick:" (get cd :nick)
                 "Real-Name:" (get cd :real-name))
        (enqueue server-output (list nil "We got the channel!"))
        (let [server (clojirc.irc/connect-to
                      (:server cd)
                      (:port cd)
                      (:nick cd)
                      (:real-name cd))
              add-fn (clojirc.irc/create-handler server)]
          (add-fn "PING" clojirc.irc/ping-handler)
;          (add-fn :all (fn [user command server] (println "[" user "] ["  command "]")))
          (add-fn :numeric clojirc.irc/numeric-handler)
          (add-fn "NOTICE" clojirc.irc/notice-handler)
          (add-fn "PRIVMSG" clojirc.irc/privmsg-handler)
          (siphon (get @server :server-out) server-output)
          (siphon
           (clojirc.irc/join-channel server "#clojure")
           (clojirc.ui/add-output-channel (get @server :server-name) "#clojure" (:window window-submit)))
          (siphon 
           (clojirc.irc/join-channel server "##linux") 
           (clojirc.ui/add-output-channel (get @server :server-name) "##linux" (:window window-submit)))))
     (:window window-submit))))



















;  (let [server (clojirc.irc/connect-to "chat.freenode.net" 6667 "jack0" "me")
;        add-fn (clojirc.irc/create-handler server)]
;    (add-fn "PING" clojirc.irc/ping-handler)
;    (add-fn :all #(println "[" %1 "] [" %2 "] [" %3 "]"))
;    (add-fn :numeric #(println "Got Numeric command:" %1 %2 %3))))

;  (def window-submit (clojirc.ui/setup-window))
  ;(clojirc.ui/register-connect-to-fn 
  ; #(do
  ;    (println "Got Connection to:\n"
  ;             "Server:" (get %1 :server)
  ;             "Port:" (get %1 :port)
  ;             "Nick:" (get %1 :nick)
  ;             "Real-Name:" (get %1 :real-name))
  ;    (enqueue %2 "We got the channel!"))
  ; window))
;  (clojirc.ui/register-connect-to-fn
;   #(let [cd %1
;          server-channel %2
;          server (clojirc.irc/connect-to
;                  (get %1 :server)
;                  (get %1 :port)
;                  (get %1 :nick)
;                  (get %1 :real-name))]
;      (clojirc.irc/add-handler "PING" #(clojirc.irc/ping-handler %1 %2 server))
;      (clojirc.irc/add-handler "PRIVMSG" #(clojirc.irc/privmsg-handler %1 %2 server))
;      (receive-all server clojirc.irc/handler)
;      (siphon (get window-submit :submit) server))
;   (get window-submit :window)))
  ;(clojirc.ui/register-submit-fn user-submited window)
  ;(clojirc.irc/start-freenode)
  ;(enqueue clojirc.irc/irc-server "JOIN ##linux"))

;  (def irc-channel (clojirc.irc/create-irc-channel "chat.freenode.net" 6667
;                                                   "jack_rabbit" "Kyle Nusbaum"))
;  (receive-all irc-channel clojirc.irc/handler)
;  (clojirc.irc/add-handler "PING" clojirc.irc/ping-handler)
;  (clojirc.irc/add-handler "PRIVMSG" message-handler))


;(defn user-submited [text]
;  (clojirc.ui/add-output (str text "\n") window))
  
;(defn message-handler [user command channel]
;  (clojirc.ui/add-output (str command "\n") window))
