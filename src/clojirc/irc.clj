(ns clojirc.irc
  (:require clojure.string)
  (:use lamina.core aleph.tcp gloss.core))

(declare separate-command-args)
(declare parse-command)
(declare parse-user)

(defn connect-to [server-ip server-port
                          nickname real-name]
  (let [irc-server (wait-for-result 
                    (tcp-client {:host server-ip
                                 :port server-port
                                 :frame (string :utf-8 :delimiters ["\r\n"])}))]
    (enqueue irc-server (str "NICK " nickname))
    (enqueue irc-server (str "USER " nickname " 0 * :" real-name))
    (atom {:server irc-server :server-out (channel) :server-name server-ip})))

(defn create-handler [server]
  (let [message-handlers (atom {})]
    (receive-all (get @server :server)
                 #(let [split-message (clojure.string/split % #"\s")
                        command (if (= (first (first split-message)) \:)
                                  (drop 1 split-message)
                                  split-message)
                        user    (if (= (first (first split-message)) \:)
                                  (first split-message)
                                  nil)
                        handler (get @message-handlers (first command))
                        all-handler (get @message-handlers :all)
                        numeric-handler (get @message-handlers :numeric)]

                    (if (not (= nil handler))
                      (handler user command server)
                      (if (not (= nil numeric-handler))
                        (try
                          (Integer/parseInt (first command))
                          (numeric-handler user command server)
                          (catch NumberFormatException e (println "No handler for [" (first command) "], and it's not numeric:" %)))
                        (println "No handler for" (first command))))
                    (when (not (= nil all-handler))
                      (all-handler user command server))))
    
    (letfn [(add-handler [command handler]
              (swap! message-handlers #(assoc % command handler)))]
      add-handler)))

(defn ping-handler [user command server]
  (let [command-params (separate-command-args command false)]
    (enqueue (:server @server) (str "PONG " (second command-params))))
  (println "Pongged!"))

(defn numeric-handler [user command server]
  (let [command-and-args (separate-command-args command)
        command (parse-command (first command-and-args))
        args    (second command-and-args)]
    (if (not (= args nil))
      (try
        (let [chan (get @server (last command))]
          (if (= chan nil)
            (throw (IndexOutOfBoundsException.)))
          (println "[notice-handler]:" "[" command "] [" args "]")
          (enqueue chan (list nil args)))
        (catch IndexOutOfBoundsException e 
            (enqueue (get @server :server-out) (list nil args)))))))

(defn notice-handler [user command server]
  (let [command-and-args (separate-command-args command)
        comm (parse-command (first command-and-args))
        args (second command-and-args)]
    (println "[notice-handler] command: [" command "]")
    (println "[notice-handler]:" "[" comm "] [" args "]")
    (enqueue (get @server :server-out) (list nil args))))

(defn privmsg-handler [user command server]
  (let [command-and-args (separate-command-args command)
        command (parse-command (first command-and-args))
        args    (second command-and-args)]
    (let [chan (get @server (last command))
          user-name (parse-user user)]
      (if (not (= nil chan))
        (enqueue chan (list user-name args))))
    (println "Got PRIVMSG: [" user "] [" command "] [" args "]")))

(defn separate-command-args 
  ([command]
     (println command)
     (let [commstr (reduce #(str %1 " " %2) command)
           index (.indexOf commstr ":")
           comm (if (>= index 0)
                  (subs commstr 0 (dec index))
                  commstr)
           args (if (>= index 0)
                  (subs commstr (inc index))
                  nil)]
       (list comm args)))
  ([command strip-colon]
     (if strip-colon
       (separate-command-args command)
       (let [commstr (reduce #(str %1 " " %2) command)
             index (.indexOf commstr ":")
             comm (if (>= index 0)
                    (subs commstr 0 (dec index))
                    commstr)
             args (if (>= index 0)
                    (subs commstr index)
                    nil)]
         (list comm args)))))

(defn parse-command [command]
  (clojure.string/split command #"\s"))

(defn parse-user [user]
  (subs user 1 (.indexOf user "!")))

(defn join-channel [server chan]
  (let [new-channel (channel)]
    (swap! server #(assoc % chan new-channel))
    (enqueue (:server @server) (str "JOIN " chan))
    new-channel))
